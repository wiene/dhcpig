Source: dhcpig
Section: net
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Philippe Thierry <phil@reseau-libre.net>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pkg-config,
               python3,
               python3-setuptools
Standards-Version: 4.6.2
Homepage: https://github.com/kamorin/DHCPig
Vcs-Git: https://salsa.debian.org/pkg-security-team/dhcpig.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/dhcpig

Package: dhcpig
Architecture: all
Depends: python3-scapy, ${misc:Depends}, ${python3:Depends}
Description: DHCP exhaustion script using scapy network library
 DHCPig initiates an advanced DHCP exhaustion attack. It will consume all IPs
 on the LAN, stop new users from obtaining IPs, release any IPs in use, then
 for good measure send gratuitous ARP and knock all windows hosts offline.
 .
 It is based on the scapy library and requests admin privileges to execute.
 It has been tested on multiple Linux distributions and multiple DHCP servers
 (ISC, Windows 2k3/2k8, ...).
